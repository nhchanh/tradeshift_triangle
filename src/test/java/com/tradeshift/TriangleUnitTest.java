package com.tradeshift;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TriangleUnitTest {

	@DataProvider
	public Object[][] getValidTriangleData() {
		return new Object[][] {
						{ 6, 6, 6, Triangle.Type.EQUILATERAL },
						{ 5, 5, 5, Triangle.Type.EQUILATERAL },
						{ 5, 5, 6, Triangle.Type.ISOSCELES },
						{ 5, 6, 5, Triangle.Type.ISOSCELES },
						{ 6, 5, 5, Triangle.Type.ISOSCELES },
						{ 4, 5, 6, Triangle.Type.SCALENE },
						{ 6, 5, 4, Triangle.Type.SCALENE },
						{ 6, 4, 5, Triangle.Type.SCALENE }
					};
	}

	@DataProvider
	public Object[][] getInvalidTriangleData() {
		return new Object[][] { { 0, 6, 6 }, { 6, 0, 0 }, { -1, 6, 6 }, { 6, -1, 6 }, { 6, 6, -1 }, };
	}

	@DataProvider
	public Object[][] getInvalidTriangleDataForTestingSetter() {
		return new Object[][] {
						{ 0 },
						{ -1 },
						{ Integer.MIN_VALUE},
					};
	}

	@DataProvider
	public Object[][] getValidTriangleDataForTestingSetter() {
		return new Object[][] {
						{ 1 },
						{ 1000 },
						{ Integer.MAX_VALUE}
					};
	}

	@Test(dataProvider = "getValidTriangleData", description = "test triangle - test all type - valid sides")
	public void testTriangleType(int side1, int side2, int side3, Triangle.Type triangleType) {
		// Given
		Triangle triangle = new Triangle(side1, side2, side3);

		// when
		Triangle.Type type = triangle.getType();

		// then
		Assert.assertEquals(type, triangleType);
	}

	@Test(dataProvider = "getInvalidTriangleData", description = "test triangle - test valid sides", expectedExceptions = IllegalArgumentException.class)
	public void testTriangleContructorWithInvalidValue(int side1, int side2, int side3) {
		// Given & Then
		@SuppressWarnings("unused")
		Triangle triangle = new Triangle(side1, side2, side3);

		// then: expecting the runtime exception IllegalArgumentException.class to be thrown
	}

	@Test(dataProvider = "getValidTriangleDataForTestingSetter", description = "test triangle - test setters / getters with valid data")
	public void testTriangleSetter1WithValidValue(int side) {
		// Given
		Triangle triangle = new Triangle(side, 10, 30);

		//then
		triangle.setSide1(side);

		// then
		Assert.assertEquals(triangle.getSide1(), side);
	}

	@Test(dataProvider = "getValidTriangleDataForTestingSetter", description = "test triangle - test setters / getters with valid data")
	public void testTriangleSetter2WithValidValue(int side) {
		// Given
		Triangle triangle = new Triangle(side, 10, 30);

		//then
		triangle.setSide2(side);

		// then
		Assert.assertEquals(triangle.getSide2(), side);
	}

	@Test(dataProvider = "getValidTriangleDataForTestingSetter", description = "test triangle - test setters / getters with valid data")
	public void testTriangleSetter3WithValidValue(int side) {
		// Given
		Triangle triangle = new Triangle(side, 10, 30);

		//then
		triangle.setSide3(side);

		// then
		Assert.assertEquals(triangle.getSide3(), side);
	}

	@Test(dataProvider = "getInvalidTriangleDataForTestingSetter", description = "test triangle - test setters / getters with valid data", expectedExceptions = IllegalArgumentException.class)
	public void testTriangleSetter1WithInvalidValue(int side) {
		// Given
		Triangle triangle = new Triangle(10, 10, 30);

		//then
		triangle.setSide1(side);

		// then: expecting the runtime exception IllegalArgumentException.class to be thrown
	}

	@Test(dataProvider = "getInvalidTriangleDataForTestingSetter", description = "test triangle - test setters / getters with valid data", expectedExceptions = IllegalArgumentException.class)
	public void testTriangleSetter2WithInvalidValue(int side) {
		// Given
		Triangle triangle = new Triangle(10, 10, 30);

		//then
		triangle.setSide2(side);

		// then: expecting the runtime exception IllegalArgumentException.class to be thrown
	}

	@Test(dataProvider = "getInvalidTriangleDataForTestingSetter", description = "test triangle - test setters / getters with valid data", expectedExceptions = IllegalArgumentException.class)
	public void testTriangleSetter3WithInvalidValue(int side) {
		// Given
		Triangle triangle = new Triangle(10, 10, 30);

		//then
		triangle.setSide3(side);

		// then: expecting the runtime exception IllegalArgumentException.class to be thrown
	}
}
