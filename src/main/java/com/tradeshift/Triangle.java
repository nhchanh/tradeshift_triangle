package com.tradeshift;

/**
 * This class represent a triagnle object
 *
 *
 */
public class Triangle {
	enum Type {
		EQUILATERAL, ISOSCELES, SCALENE
	}

	private int side1;
	private int side2;
	private int side3;


	public Triangle(int side1, int side2, int side3) {
		super();
		vadidateSide(side1);
		vadidateSide(side2);
		vadidateSide(side3);
		this.side1 = side1;
		this.side2 = side2;
		this.side3 = side3;
	}

	private void vadidateSide(int side) {
		if(side <= 0) {
			throw new IllegalArgumentException("One side of the triangle is invalid: " + side);
		}
	}

	public Type getType() {
		if(this.side1 == this.side2 && this.side2 == this.side3) {
			return Type.EQUILATERAL;
		}
		if(this.side1 == this.side2 || this.side2 == this.side3 || this.side1 == this.side3) {
			return Type.ISOSCELES;
		}

		return Type.SCALENE;
	}

	public int getSide1() {
		return side1;
	}


	public void setSide1(int side1) {
		vadidateSide(side1);
		this.side1 = side1;
	}

	public int getSide2() {
		return side2;
	}


	public void setSide2(int side2) {
		vadidateSide(side2);
		this.side2 = side2;
	}

	public int getSide3() {
		return side3;
	}

	public void setSide3(int side3) {
		vadidateSide(side3);
		this.side3 = side3;
	}
}